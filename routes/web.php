<?php

//Route::view('/', 'main');
Route::get('/', 'LandingPageController@index')->name('main');
Route::get('/shop', 'ShopController@index')->name('shop.index');

Route::get('/shop/{product}', 'ShopController@show')->name('shop.show');

//Route::view('/products', 'products');
Route::view('/product', 'product');
Route::get('/cart', 'CartController@index')->name('cart.index');
Route::post('/cart', 'CartController@store')->name('cart.store');
Route::delete('/cart/{product}', 'CartController@destroy')->name('cart.destroy');
Route::patch('/cart/{product}', 'CartController@update')->name('cart.update');

Route::post('/cart/switchToSaveForLater/{product}', 'CartController@switchToSaveForLater')->name('cart.switchToSaveForLater');

Route::delete('/deseos/{product}', 'DeseoController@destroy')->name('deseo.destroy');
Route::post('/deseos/moverAlCarrito/{product}', 'DeseoController@siwtchToCart')->name('deseo.moverAlCarrito');

Route::post('/cupon', 'CuponController@store')->name('cupon.store');
Route::delete('/cupon', 'CuponController@destroy')->name('cupon.destroy');

Route::get('/checkout', 'CheckoutController@index')->name('checkout.index')->middleware('auth');
Route::get('/guestCheckout', 'CheckoutController@index')->name('guestCheckout.index');
Route::view('/thankyou', 'thankyou');

Route::get('/paypal', function(){
    return view('paypal');
});


Route::get('/empty', function(){
    Cart::instance('saveForLater')->destroy();
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
