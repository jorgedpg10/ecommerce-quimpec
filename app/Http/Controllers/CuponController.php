<?php

namespace App\Http\Controllers;

use App\Cupon;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;

class CuponController extends Controller
{




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $cupon = Cupon::where('codigo', $request->nombre_cupon)->first();

        if( !$cupon ){
            return redirect()->route('checkout.index')->withErrors( 'No es un código válido');
        }

        session()->put('cupon', [
            'nombre' => $cupon->codigo,
            'descuento' => $cupon->descuento( Cart::subtotal(2, ".", "") ),

        ]);

        return redirect()->route('checkout.index')->with('success_message', 'El cupón han sido aplicado!');
    }


    public function destroy()
    {
        session()->forget('cupon');

        return redirect()->route('checkout.index')->with('success_message', 'El cupón se ha quitado');
    }
}
