<?php

namespace App\Http\Controllers;

use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;

class DeseoController extends Controller
{

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::instance('saveForLater')->remove($id);

        return route('cart.index')->with('success_message', 'Se ha removido de su lista de Deseos');
    }

    /**
     *  Moves the item to cart
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function siwtchToCart($id)
    {

        $item = Cart::instance('saveForLater')->get($id);
        Cart::instance('saveForLater')->remove($id);

        $duplicates = Cart::instance('default')->search(function ($cartItem, $rowId) use ($id) {
            return $rowId === $id;
        });

        if( $duplicates->isNotEmpty()){
            return redirect()->route('cart.index')->with('success_message', 'Producto ya está en su carrito');

        }

        Cart::instance('default')->add($item->id, $item->name, 1, $item->price)
            ->associate('App\Product');

        return redirect()->route('cart.index')->with('success_message', 'Producto guardado en su carrito');
    }
}
