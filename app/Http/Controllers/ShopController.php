<?php

namespace App\Http\Controllers;

use App\Categoria;
use App\Product;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pagination = 9;
        $categorias = Categoria::all();

        if (request()->categoria) {
            $products = Product::with('categoria')->whereHas('categoria', function ($query) {
                $query->where('slug', request()->categoria);
            });

            $nombre_categoria = optional( $categorias->where('slug', request()->categoria)->first() )->name; //devuelve null en vez de botar un error
        } else {
            $products = Product::where('recomendado', true);
            $nombre_categoria = 'Recomendados';
        }

        if (request()->sort == 'ascendente') {
            $products = $products->orderBy('price')->paginate($pagination);
        } elseif (request()->sort == 'descendente') {
            $products = $products->orderBy('price', 'desc')->paginate($pagination);
        }else{
            $products = $products->paginate($pagination);
        }

        return view('shop')->with([
            'products' => $products,
            'categorias' => $categorias,
            'nombre_categoria' => $nombre_categoria
        ]);
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);

        $migth_also_like = Product::where('id', '!=', $id)->migthAlsoLike()->get();

        return view('product')->with([
            'product' => $product,
            'migth_also_like' => $migth_also_like
        ]);
    }


}
