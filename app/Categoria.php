<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Categoria extends Model
{
    public function producto() {
        return $this->belongsToMany('App\Product', 'categoria_producto');
    }
}
