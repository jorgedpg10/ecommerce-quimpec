<?php

/*function formatoDolar($price){
    return '$'. floor( ((float)$price) )/100;

}*/

function cast_de_string_a_float($price){
    return  floor( (float)$price) ;
}

function acota_en_dos_decimales($price){
    return (float)(number_format( $price, 2, '.', ''));
}

/*
 * recibe precio en formato de Stripe (x100)
 * devuelve formateado para mostrar al usuario
 * */
function formato_dolar_humano ($precio){
    $valor = $precio /100;
    return '$'. number_format( $valor, 2, '.', ',');
}

