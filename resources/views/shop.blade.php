@extends('layout')

@section('title', 'Products')

@section('extra-css')

@endsection

@section('content')

    <div class="breadcrumbs">
        <div class="container">
            <a href="/">Home</a>
            <span><i class="fas fa-angle-right"></i></span>
            <span>Shop</span>
        </div>
    </div> <!-- end breadcrumbs -->

    <div class="products-section container">
        <div class="sidebar">
            <h3>By Category</h3>
            <ul>
                @foreach($categorias as $categoria)
                    <li class="{{ request()->categoria == $categoria->slug ? 'active' : '' }}">
                        <a href=" {{ route('shop.index', ['categoria' => $categoria->slug]) }}">{{ $categoria->name }}</a>
                    </li>
                @endforeach
            </ul>

        </div> <!-- end sidebar -->

        <div>
            <div class="products-header">
                <h1 class="stylish-heading">{{ $nombre_categoria }}</h1>
                <div>
                    <strong>Precio:</strong>
                    <a href=" {{ route('shop.index', ['categoria' => request()->categoria, 'sort' => 'ascendente']) }}">Ascendente</a> |
                    <a href=" {{ route('shop.index', ['categoria' => request()->categoria, 'sort' => 'descendente']) }}">Descendente</a>
                </div>
            </div>
            <div class="products text-center">
                @forelse($products as $product)

                    <div class="product">
                        <a href="{{ route('shop.show', $product->id) }}"><img
                                    src="{{asset('storage/'.$product->image) }}" alt="product"></a>
                        <a href="{{ route('shop.show', $product->id) }}">
                            <div class="product-name">{{$product->name }}</div>
                        </a>
                        <div class="product-price">{{ $product->asDollars() }}</div>
                    </div>
                @empty
                    <div>No encontramos artículos</div>
                @endforelse
            </div> <!-- end products -->
            <div class="spacer"></div>

            {{ $products->appends( request()->input() )->links() }}
        </div>
    </div>


@endsection
