<header>
    <div class="top-nav container">
        <div class="top-nav-left">
            {{--@if (! request()->is('checkout'))--}}
                <ul>
                    <li class="logo"><a href="/"><img  src=" {{ asset('img/logo-quimpec.png') }}" alt="logo quimpec" ></a></li>
                    <li><a href="{{route('shop.index')}}">Tienda</a></li>
                    <li><a href="#">Nosotros</a></li>
                    <li><a href="#">Blog</a></li>
                </ul>
            {{--@endif--}}
        </div>

        <div class="top-nav-rigth">
            @include('partials.menus.main-rigth')
        </div>
    </div>

</header>
