<ul>
    @guest
        <li><a href="{{ route('register') }}">Registrarse</a></li>
        <li><a href="{{ route('login') }}">Entrar</a></li>
    @else
        <li>
            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">
                Cerrar Sesión
            </a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    @endguest
    <li><a href="{{route('cart.index')}}"><i class="fas fa-shopping-cart"></i>
            @if( Cart::instance('default')->count() > 0)
                <span class="cart-count"><span>{{ Cart::instance('default')->count() }}</span></span>
            @endif
        </a></li>
</ul>
