<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Quimpec Químicas | @yield('title', '')</title>

    <link href="/img/favicon.ico" rel="SHORTCUT ICON"/>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat%7CRoboto:300,400,700" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">

    @yield('extra-css')
</head>


<body class="@yield('body-class', '')">
<div id="app">
    @include('partials.nav')

    @yield('content')


    @include('partials.footer')
</div>
    @yield('extra-js')

</body>
</html>
