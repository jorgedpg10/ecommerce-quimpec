@extends('layout')

@section('title', 'Shopping Cart')

@section('extra-css')

@endsection

@section('content')

    <div class="breadcrumbs">
        <div class="container">
            <a href="#">Home</a>
            <i class="fa fa-chevron-right breadcrumb-separator"></i>
            <span>Shopping Cart</span>
        </div>
    </div> <!-- end breadcrumbs -->

    <div class="cart-section container">
        <div>


            @if( session()->has('success_message') )
                <div class="alert alert-success">
                    {{ session()->get('success_message') }}
                </div>
            @endif

            @if( count($errors) >0 )
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @if( Cart::count() > 0)



                <h2>Tienes {{Cart::count() }} producto(s) en el carrito</h2>

                <div class="cart-table">
                    @foreach(Cart::content() as $item)
                        <div class="cart-table-row">
                            <div class="cart-table-row-left">
                                <a href="{{ route('shop.show', $item->id) }}"><img
                                            src="{{asset('storage/'.$item->model->image) }}" alt="item"
                                            class="cart-table-img"></a>
                                <div class="cart-item-details">
                                    <div class="cart-table-item"><a
                                                href=" {{ route('shop.show', $item->id) }}">{{ $item->name }}</a></div>
                                    <div class="cart-table-description">{{$item->model->details}}</div>
                                </div>
                            </div>
                            <div class="cart-table-row-right">
                                <div class="cart-table-actions">
                                    <form action="{{ route('cart.destroy', $item->rowId) }}" method="POST"
                                          class="form-quitar">
                                        {{csrf_field()}}
                                        {{ method_field('DELETE') }}

                                        <button type="submit" class="cart-options">Quitar</button>
                                    </form>

                                    <form action="{{ route('cart.switchToSaveForLater', $item->rowId) }}" method="POST">
                                        {{csrf_field()}}
                                        <button type="submit" class="cart-options">Guardar en Lista de deseos</button>
                                    </form>

                                </div>
                                <div>
                                    <select class="quantity" data-id="{{ $item->rowId }}">
                                        @for($i = 1; $i <=10 ; $i++ )
                                            <option {{ $item->qty == $i ? 'selected' : '' }}>{{$i}}</option>
                                        @endfor

                                        {{--<option {{ $item->qty == 1 ? 'selected' : ''  }}>1</option>
                                        <option {{ $item->qty == 2 ? 'selected' : ''  }}>2</option>
                                        <option {{ $item->qty == 3 ? 'selected' : ''  }}>3</option>
                                        <option {{ $item->qty == 4 ? 'selected' : ''  }}>4</option>
                                        <option {{ $item->qty == 5 ? 'selected' : ''  }}>5</option>--}}
                                    </select>
                                </div>

                                <div>{{ formato_dolar_humano( $item->subtotal ) }}</div>  <!--Precio-->
                            </div>
                        </div> <!-- end cart-table-row -->
                    @endforeach
                </div> <!-- end cart-table -->



                <div class="cart-totals">
                    <div class="cart-totals-left">
                        El envío es gratis
                    </div>

                    <div class="cart-totals-right">
                        <div>
                            Subtotal <br>
                            Iva <br>
                            <span class="cart-totals-total">Total</span>
                        </div>
                        <div class="cart-totals-subtotal">
                            {{ formato_dolar_humano( Cart::subtotal(2, ".", "")) }}
                            {{ formato_dolar_humano( Cart::tax(2, ".", "") ) }} <br>
                            <span class="cart-totals-total">{{ formato_dolar_humano(Cart::total(2, ".", "")) }}</span>
                        </div>
                    </div>
                </div> <!-- end cart-totals -->

                <div class="cart-buttons">
                    <a href="#" class="button">Seguir Comprando</a>
                    <a href=" {{ route('checkout.index') }}" class="button-primary">Proceder a Pagar</a>
                </div>

            @else
                <h2>Su carrito está vacío</h2>
            @endif

            @if( Cart::instance('saveForLater')->count() > 0)



                <h2>Tiene {{Cart::instance('saveForLater')->count() }} producto(s) en su lista de deseos</h2>

                <div class="saved-for-later cart-table">
                    @foreach( Cart::instance('saveForLater')->content() as $item)
                        <div class="cart-table-row">
                            <div class="cart-table-row-left">
                                <a href="{{ route('shop.show', $item->id) }}"><img
                                            src="{{asset('storage/'.$item->model->image) }}" alt="item"
                                            class="cart-table-img"></a>
                                <div class="cart-item-details">
                                    <div class="cart-table-item"><a
                                                href=" {{ route('shop.show', $item->id) }}">{{ $item->name }}</a></div>
                                    <div class="cart-table-description">{{$item->model->details}}</div>
                                </div>
                            </div>
                            <div class="cart-table-row-right">
                                <div class="cart-table-actions">
                                    <form action="{{ route('deseo.destroy', $item->rowId) }}" method="POST">
                                        {{csrf_field()}}
                                        {{ method_field('DELETE') }}

                                        <button type="submit" class="cart-options">Quitar</button>
                                    </form>

                                    <form action="{{ route('deseo.moverAlCarrito', $item->rowId) }}" method="POST">
                                        {{csrf_field()}}
                                        <button type="submit" class="cart-options">Mover al carrito</button>
                                    </form>
                                </div>
                                {{-- <div>
                                    <select class="quantity">
                                        <option selected="">1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div> --}}
                                <div>{{ $item->model->asDollars() }}</div>
                            </div>
                        </div> <!-- end cart-table-row -->
                    @endforeach

                </div> <!-- end saved-for-later -->
            @else
                <h3>Su lista de deseos está vacía</h3>
            @endif
        </div>

    </div> <!-- end cart-section -->

    @include('partials.might-like')


@endsection

@section('extra-js')
    <script src="{{ asset(('js/app.js')) }}"></script>
    <script>
        (function () {
            const classname = document.querySelectorAll('.quantity');

            Array.from(classname).forEach(function (element) {
                element.addEventListener('change', function () {
                    const id = element.getAttribute('data-id');
                    axios.patch(`/cart/${id}`, {
                        quantity: this.value
                    })
                        .then(function (response) {
                            //console.log(response);
                            window.location.href = '{{ route('cart.index') }}'
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                })
            })
        })();
    </script>
@endsection
































