@extends('layout')

@section('title', $product->name)

@section('extra-css')

@endsection

@section('content')

    <div class="breadcrumbs">
        <div class="container">
            <a href="/">Home</a>
            <i class="fa fa-chevron-right breadcrumb-separator"></i>
            <a href="{{route('shop.index')}}"><span>Shop</span></a>
            <i class="fa fa-chevron-right breadcrumb-separator"></i>
            <span>{{ $product->name }}</span>
        </div>
    </div> <!-- end breadcrumbs -->

    <div class="product-section container">
        <quimpec-producto :producto=" {{ $product->toJson() }} " ></quimpec-producto>
{{--        <div>
            <div class="product-section-image">
                <img src="{{ asset('storage/'.$product->image) }}" alt="product">
            </div>
            <div class="product-section-images">
                <div class="product-section-thumbnail selected">
                    <img src="{{ asset('img/blog1.png') }}" alt="product">
                </div>

                <div class="product-section-thumbnail">
                    <img src="{{ asset('img/blog2.png') }}" alt="product">
                </div>
                @if( $product->images )
                    @foreach( json_decode($product->images) as $image )

                        <img src="{{ asset('storage/'.$image) }}" alt="product">
                    @endforeach
                @endif
            </div>
        </div>
        --}}
        <div class="product-section-information">
            <h1 class="product-section-title">{{ $product->name }}</h1>
            <div class="product-section-subtitle">{{ $product->details }}</div>
            <div class="product-section-price">{{ $product->asDollars() }}</div>

            <p>
                {!! $product->description !!}
            </p>

            <form action="{{ route('cart.store') }}" method="POST">
                {{csrf_field()}}
                <input type="hidden" name="id" value="{{ $product->id }}">
                <input type="hidden" name="name" value="{{ $product->name }}">
                <input type="hidden" name="price" value="{{ $product->price }}">
                <button type="submit" class="button button-plain">Add to cart</button>
            </form>
        </div>
    </div> <!-- end product-section -->

    @include('partials.might-like')


@endsection

@section('extra-js')
    <script src="{{ asset('js/app.js') }}"></script>
@endsection

















